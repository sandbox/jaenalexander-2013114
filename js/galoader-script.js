(function($) {
  /**
   * Drupal behaviors on GLader
   */
  Drupal.behaviors.glader = {
    attach: function (context, settings) {           
      var selector; // Selector buffer
      var height;   // Element height

      // Set galoader box selector
      selector = '#wrapper-galoader-box:not(.galoader-processed)';      
      
      // Set galoader plugin
      $(selector).addClass('galoader-processed').glader();
    }
  }; 

  /**
   * Plugin GLader, any element appears and disappers on ajax event start
   * and ajax event stop.
   * @param (callback) onAjaxStart
   * @param (callback) onAjaxStop
   */
  $.fn.glader = function(onApply, onAjaxStart, onAjaxStop) {
    // Callback on plugin Apply
    if (typeof(onApply) === 'function') {
      onApply.call(this);
    }    

    // Glader on ajax start
    $(this).ajaxStart(function() {
      $(this).fadeIn('fast');
      
      // Callback      
      if (typeof(onAjaxStart) === 'function') {
        onAjaxStart.call(this);
      }      
    });

    // Glader on ajax stop
    $(this).ajaxStop(function() {
      $(this).fadeOut('slow');
      
      // Callback      
      if (typeof(onAjaxStop) === 'function') {
        onAjaxStop.call(this);
      }
    });
  };

})(jQuery);